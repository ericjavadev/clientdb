package com.eric.clientdb;

import org.modelmapper.ModelMapper;

import lombok.Data;

public class ModelMapperLombokTest {

	@Data
	static class ClientDtoTest {
		private Long id;
		private String firstname;
		private String Lastname;
	}
	
	@Data
	static class ClientTest {
		private Long id;
		private String firstname;
		private String lastname;
		private String testin123;
	}
	public static void main(String[] args) {
		ClientDtoTest clientdto = new ClientDtoTest();
		clientdto.setId(new Long(1));
		clientdto.setFirstname("Eric");
		clientdto.setLastname("Holloway");
		
		ModelMapper modelMapper = new ModelMapper();
		ClientTest client = modelMapper.map(clientdto, ClientTest.class);
		
		System.out.println(client.getFirstname());

	}

}
