package com.eric.clientdb.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.eric.clientdb.controller.exception.InvalidArgumentException;
import com.eric.clientdb.controller.exception.ShootException;
import com.eric.clientdb.controller.exception.ShootNotFoundException;
import com.eric.clientdb.model.Shoot;
import com.eric.clientdb.model.views.Views;
import com.eric.clientdb.service.ShootService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController

@RequestMapping(value = "/api",
	consumes = {APPLICATION_JSON_VALUE},
	produces = {APPLICATION_JSON_VALUE})
@ResponseBody
public class ShootController {

	@Resource
	ShootService shootService;
	
	@Resource
	ModelMapper modelMapper;
	
	@Resource 
	ObjectMapper mapper;
	
	// health check
	@GetMapping("/shoot/health-check")
	public ResponseEntity<Object> healthCheck() {
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}
	
    // create new shoot
    @PostMapping("/shoot")
    public ResponseEntity<Object> saveshoot(@RequestBody Shoot shoot) throws JsonProcessingException {
    	
        if(shoot == null) {
            throw new InvalidArgumentException("shoot json cannot be empty.");
        }
  
        shoot = shootService.createShoot(shoot);
        if(shoot == null) {    	
        	throw new DataIntegrityViolationException("shoot already exists.");
        }
         
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(shoot);
        
        Map<String, String> response = new HashMap<String,String>();
        response.put("shoot", responseJson);
        
        return ResponseEntity.status(HttpStatus.OK).body(response); 
    }
    
	// update shoot
	@PutMapping("/shoot")
	public ResponseEntity<Object> updateShoot(@RequestBody Shoot shoot) throws JsonProcessingException {
		
		if(shoot == null) {
			throw new InvalidArgumentException("shoot json required.");
		}

		shoot = shootService.updateShoot(shoot);
		if(shoot == null) {
			throw new ShootException("shoot was not saved");
		}
			
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(shoot);
        
        Map<String, String> response = new HashMap<String,String>();
        response.put("shoot", responseJson);

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	// get shoot by id
    @GetMapping("/shoot/{shootId}")
    public ResponseEntity<Object> getShoot(@PathVariable(value = "shootId") Long shootId) throws JsonProcessingException {
        
        Shoot shoot = shootService.findShootById(shootId);
        if(shoot == null) {
        	throw new ShootNotFoundException("shoot not found for id: " + shootId);
        }
        
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(shoot);

        Map<String, String> response = new HashMap<String,String>();
        response.put("shoot", responseJson);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    
	// get shoot list
	@GetMapping("/shoot/list")
	public ResponseEntity<Object> getAllShoots() throws JsonProcessingException {
		Iterable<Shoot> shootList = shootService.findAllShoots();

		if(shootList == null) {
			throw new ShootNotFoundException("no shoots found.");
		}
		
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(shootList);
        
        Map<String, String> response = new HashMap<String,String>();
        response.put("shootList", responseJson);
        
        return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	// delete shoot
    @DeleteMapping("/shoot/{shootId}")
    public ResponseEntity<Object> deleteshoot(@PathVariable(value = "shootId") Long shootId) {

        // check if shoot exists
        if(!shootService.shootExists(shootId)) {
        	throw new ShootNotFoundException("shoot does not exist for id: " + shootId);
        }
        
        shootService.deleteShoot(shootId);

        return ResponseEntity.status(HttpStatus.OK).body(null);
    }	
}
