package com.eric.clientdb.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.eric.clientdb.controller.exception.ClientException;
import com.eric.clientdb.controller.exception.ClientNotFoundException;
import com.eric.clientdb.controller.exception.InvalidArgumentException;
import com.eric.clientdb.model.Client;
import com.eric.clientdb.model.views.Views;
import com.eric.clientdb.service.ClientService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController

@RequestMapping(value = "/api",
	consumes = {APPLICATION_JSON_VALUE},
	produces = {APPLICATION_JSON_VALUE})
@ResponseBody
public class ClientController {

	@Resource
	private ClientService clientService;
	
	@Resource
	private ModelMapper modelMapper;
	
	@Resource 
	ObjectMapper mapper;
    
	// health check
	@GetMapping("/client/health-check")
	public ResponseEntity<Object> healthCheck() {
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}
	
    // create new client
    @PostMapping("/client")
    public ResponseEntity<Object> saveClient(@RequestBody Client client) throws JsonProcessingException {
    	
        if(client == null) {
            throw new InvalidArgumentException("client json cannot be empty.");
        }

        client = clientService.createClient(client);
        if(client == null) {    	
        	throw new DataIntegrityViolationException("client already exists.");
        }
	         
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(client);
        
        Map<String, String> response = new HashMap<String,String>();
        response.put("client", responseJson);

        return ResponseEntity.status(HttpStatus.OK).body(response); 
    }
    
    // update client
    @PutMapping("/client")
    public ResponseEntity<Object> updateClient(@RequestBody Client client) throws JsonProcessingException {

        if(client == null) {
        	throw new InvalidArgumentException("client json must be included.");
        }

        if(client.getId() == null) {
        	throw new InvalidArgumentException("clientId must be set.");
        }    
        
        // check if client exists
        if(!clientService.clientExists(client.getId())) {
        	throw new ClientNotFoundException("client does not exist for id: " + client.getId());
        }
        
        client = clientService.updateClient(client);
        if(client == null) {
        	throw new ClientException("client was not saved");
        }
        
    	String responseJson = mapper
    			.writerWithView(Views.Public.class)
    			.writeValueAsString(client);
        
    	Map<String, String> response = new HashMap<String,String>();
        response.put("client", responseJson);

    	return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    
	// get client by id
    @GetMapping("/client/{clientId}")
    public ResponseEntity<Object> getClient(@PathVariable(value = "clientId") Long clientId) throws JsonProcessingException {

        Client client = clientService.findClientById(clientId);
        if(client == null) {
        	throw new ClientNotFoundException("client not found for id: " + clientId);
        }
        
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(client);

        Map<String, String> response = new HashMap<String,String>();
        response.put("client", responseJson);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    
	// get client list
	@GetMapping("/client/list")
	public ResponseEntity<Object> getAllClients() throws JsonProcessingException {
		Iterable<Client> clientList = clientService.findAllClients();

    	String responseJson = mapper
    			.writerWithView(Views.Public.class)
    			.writeValueAsString(clientList);
        
        Map<String, String> response = new HashMap<String,String>();
	    response.put("clientList", responseJson);
        return ResponseEntity.status(HttpStatus.OK).body(response);
	}

    // delete client
    @DeleteMapping("/client/{clientId}")
    public ResponseEntity<Object> deleteClient(@PathVariable(value = "clientId") Long clientId) {

        // check if client exists
        if(!clientService.clientExists(clientId)) {
        	throw new ClientNotFoundException("client does not exist for id: " + clientId);
        }
        
        clientService.deleteClient(clientId);

        return ResponseEntity.status(HttpStatus.OK).body(null);
    } 
}

