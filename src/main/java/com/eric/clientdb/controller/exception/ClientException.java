package com.eric.clientdb.controller.exception;

public class ClientException extends RuntimeException {
	public ClientException(String exception) {
		super(exception);
	}

}
