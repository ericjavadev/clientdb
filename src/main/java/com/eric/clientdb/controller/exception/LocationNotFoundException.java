package com.eric.clientdb.controller.exception;

public class LocationNotFoundException extends RuntimeException {
	  public LocationNotFoundException(String exception) {
		    super(exception);
		  }

}
