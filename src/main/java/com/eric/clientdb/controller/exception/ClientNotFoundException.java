package com.eric.clientdb.controller.exception;

public class ClientNotFoundException extends RuntimeException {
	  public ClientNotFoundException(String exception) {
		    super(exception);
		  }
}
