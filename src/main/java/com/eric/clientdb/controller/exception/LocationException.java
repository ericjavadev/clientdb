package com.eric.clientdb.controller.exception;

public class LocationException extends RuntimeException {
	public LocationException(String exception) {
		super(exception);
	}

}
