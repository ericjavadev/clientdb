package com.eric.clientdb.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController

@RequestMapping(value = "/api",
consumes = {APPLICATION_JSON_VALUE},
produces = {APPLICATION_JSON_VALUE})

public class SearchController {

	// health check
	@GetMapping("/search/health-check")
	public ResponseEntity<Object> healthCheck() {		
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}
}
