package com.eric.clientdb.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.eric.clientdb.controller.exception.InvalidArgumentException;
import com.eric.clientdb.controller.exception.LocationException;
import com.eric.clientdb.controller.exception.LocationNotFoundException;
import com.eric.clientdb.model.Location;
import com.eric.clientdb.model.views.Views;
import com.eric.clientdb.service.LocationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController

@RequestMapping(value = "/api",
	consumes = {APPLICATION_JSON_VALUE},
	produces = {APPLICATION_JSON_VALUE})
@ResponseBody
public class LocationController {

	@Resource
	private LocationService locationService;
	
	@Resource
	private ModelMapper modelMapper;
	
	@Resource
	ObjectMapper mapper;
	
	// health check
	@GetMapping("/location/health-check")
	public ResponseEntity<Object> healthCheck() {
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}
	
	// create new location
    @PostMapping("/location")
    public ResponseEntity<Object> saveLocation(@RequestBody Location location) throws JsonProcessingException {
    	
        if(location == null) {
            throw new InvalidArgumentException("location json cannot be empty.");
        }
        
        location = locationService.createLocation(location);
        if(location == null) {    	
        	throw new DataIntegrityViolationException("location already exists.");
        }
        
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(location);
        
        Map<String, String> response = new HashMap<String,String>();
        response.put("location", responseJson);

        return ResponseEntity.status(HttpStatus.OK).body(response); 
    }
    
    // update location
    @PutMapping("/location")
    public ResponseEntity<Object> updateLocation(@RequestBody Location location) throws JsonProcessingException {

        if(location == null) {
        	throw new InvalidArgumentException("location json must be included.");
        }

        if(location.getId() == null) {
        	throw new InvalidArgumentException("locationId must be set.");
        }
        
        // check if client exists
        if(!locationService.locationExists(location.getId())) {
        	throw new LocationNotFoundException("location does not exist for id: " + location.getId());
        }
        
        location = locationService.updateLocation(location);
        if(location == null) {
        	throw new LocationException("location was not saved");
        } 
        
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(location);

        Map<String, String> response = new HashMap<String,String>();
        response.put("location", responseJson);

    	return ResponseEntity.status(HttpStatus.OK).body(response);
    } 
    
    // get location by id
    @GetMapping("/location/{locationId}")
    public ResponseEntity<Object> getLocation(@PathVariable(value = "locationId") Long locationId) throws JsonProcessingException {
        
        Location location = locationService.findLocationById(locationId);
        if(location == null) {
        	throw new LocationNotFoundException("location not found for id: " + locationId);
        }
	        
        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(location);

        Map<String, String> response = new HashMap<String,String>();
        response.put("location", responseJson);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    
    // get location list
	@GetMapping("/location/list")
	public ResponseEntity<Object> getAllLocations() throws JsonProcessingException {
		Iterable<Location> locationList = locationService.findAllLocations();

        String responseJson = mapper
        		.writerWithView(Views.Public.class)
        		.writeValueAsString(locationList);

        Map<String, String> response = new HashMap<String,String>();
        response.put("locationList", responseJson);
        
        return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
    // delete location
    @DeleteMapping("/location/{locationId}")
    public ResponseEntity<Object> deleteLocation(@PathVariable(value = "locationId") Long locationId) {

        // check if location exists
        if(!locationService.locationExists(locationId)) {
        	throw new LocationNotFoundException("location does not exist for id: " + locationId);
        }
        
        locationService.deleteLocation(locationId);

        return ResponseEntity.status(HttpStatus.OK).body(null);
    } 
}
