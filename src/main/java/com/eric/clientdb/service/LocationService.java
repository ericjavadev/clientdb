package com.eric.clientdb.service;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.eric.clientdb.model.Location;
import com.eric.clientdb.model.repository.LocationRepository;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@Service
public class LocationService {

	@Resource
	LocationRepository locationRepository;
	
	// create new location
	public Location createLocation(Location location) {
		try {
			locationRepository.save(location);			
		} catch(DataAccessException ex) {
			log.error("createLocation::DataAccessException ->", ex);
			location = null;
		}
		return location;
	}
	
	// update location
	public Location updateLocation(Location location) {
		try {
			locationRepository.save(location);			
		} catch(DataAccessException ex) {
			log.error("updateLocation::DataAccessException ->", ex);
			location = null;
		}
		return location;
	}
	
	// get location by locationId
	public Location findLocationById(Long id) {
		Optional<Location> location = locationRepository.findById(id);
		if(!location.isPresent()) {
			log.info("location not found with id: " + id);
			return null;
		}
		return location.get();
	}
	
	// find all locations
	public Iterable<Location> findAllLocations() {
		Iterable<Location> locationList = locationRepository.findAll();
		return locationList;
	}

	// delete location
	public void deleteLocation(Long id) {
		locationRepository.deleteById(id);
	}
	
	// check location exists
	public boolean locationExists(Long id) {
		return locationRepository.existsById(id);
	}
}
