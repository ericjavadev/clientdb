package com.eric.clientdb.service;

import java.util.Optional;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.eric.clientdb.model.Client;
import com.eric.clientdb.model.repository.ClientRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClientService {
			
	@Resource
	private ClientRepository clientRepository;
	
	// create new client
	@Transactional
	public Client createClient(Client client) {
		try {
			clientRepository.save(client);		
		} catch(DataAccessException ex) {
			log.error("createClient::DataAccessException ->", ex);
			client = null;
		}
		return client;
	}
	
	// update client
	public Client updateClient(Client client) {
		try {
			client = clientRepository.save(client);			
		} catch(DataAccessException ex) {
			log.error("updateClient::DataAccessException ->", ex);
			client = null;
		}
		return client;
	}
	
	// get client by clientId
	public Client findClientById(Long clientId) {
		Optional<Client> client = clientRepository.findById(clientId);
		if(!client.isPresent()) {
			log.info("client not found with id: " + clientId);
			return null;
		}
		return client.get();
	}
	
	// get list of all clients
	public Iterable<Client> findAllClients() {
		Iterable<Client> clientList = clientRepository.findAll();
		return clientList;
	}
	
	// delete client
	public void deleteClient(Long clientId) {
		clientRepository.deleteById(clientId);
	}
	
	// check client exists
	public boolean clientExists(Long clientId) {
		return clientRepository.existsById(clientId);
	}
}
