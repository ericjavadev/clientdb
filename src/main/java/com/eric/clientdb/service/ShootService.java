package com.eric.clientdb.service;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.eric.clientdb.model.Client;
import com.eric.clientdb.model.Shoot;
import com.eric.clientdb.model.repository.ShootRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ShootService {

	@Resource
	private ShootRepository shootRepository;
	
	// create new shoot
	public Shoot createShoot(Shoot shoot) {
		try {
			shootRepository.save(shoot);		
		} catch(DataAccessException ex) {
			log.error("createShoot::DataAccessException ->", ex);
			shoot = null;
		}
		return shoot;
	}
	
	// update shoot
	public Shoot updateShoot(Shoot shoot) {
		try {
			shoot = shootRepository.save(shoot);			
		} catch(DataAccessException ex) {
			log.error("updateShoot::DataAccessException ->", ex);
			shoot = null;
		}
		return shoot;
	}
	
	// get shoot by shootId
	public Shoot findShootById(Long shootId) {
		Optional<Shoot> shoot = shootRepository.findById(shootId);
		if(!shoot.isPresent()) {
			log.info("shoot not found with id: " + shootId);
			return null;
		}
		return shoot.get();
	}
	
	// get list of all shoots
	public Iterable<Shoot> findAllShoots() {
		Iterable<Shoot> shootList = shootRepository.findAll();
		return shootList;
	}
	
	// delete shoot
	public void deleteShoot(Long shootId) {
		shootRepository.deleteById(shootId);
	}
	
	// check shoot exists
	public boolean shootExists(Long shootId) {
		return shootRepository.existsById(shootId);
	}	
}
