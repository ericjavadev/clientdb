package com.eric.clientdb;

import org.modelmapper.ModelMapper;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.eric.clientdb"})
@EnableJpaRepositories(basePackages = "com.eric.clientdb.model.repository")
@EnableWebMvc
//@Lazy
//@EnableRetry
public class ClientdbApplication {
	
    public static void main(String[] args) {
        SpringApplication.run(ClientdbApplication.class, args);  
        log.info("application now started!!!");
    }
    
    @Bean
    protected RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ObjectMapper objectMapper() {
    	ObjectMapper mapper = new ObjectMapper();
    	mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);
        return mapper;
    }
    
    @Bean
    public ModelMapper modelMapper() {
    	return new ModelMapper();
    }
    
//    @Bean
//    protected RetryTemplate remoteServiceRetryTemplate() {
//        RetryTemplate retryTemplate = new RetryTemplate();
//        retryTemplate.setBackOffPolicy(new ExponentialRandomBackOffPolicy());
//        retryTemplate.setRetryPolicy(new SimpleRetryPolicy(5, simpleRetryExceptionClasses()));
//
//        return retryTemplate;
//    }
//
//    private static Map<Class<? extends Throwable>, Boolean> simpleRetryExceptionClasses() {
//        return Collections.unmodifiableMap(Stream.of(
//                    new AbstractMap.SimpleEntry<Class<? extends Throwable>, Boolean>(ResourceAccessException.class, Boolean.TRUE))
//                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
//    }
//
//    @Bean
//    protected RetryOperationsInterceptor remoteServiceRetryInterceptor() {
//        RetryOperationsInterceptor interceptor = new RetryOperationsInterceptor();
//        interceptor.setRetryOperations(remoteServiceRetryTemplate());
//        interceptor.setLabel("remoteServiceRetryInterceptor");
//
//        return interceptor;
//    }


}