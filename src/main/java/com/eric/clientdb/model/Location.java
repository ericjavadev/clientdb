package com.eric.clientdb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.eric.clientdb.model.views.Views;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Entity
@Table(name = "location")
public class Location {
	
	@JsonView(Views.Public.class)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@JsonView(Views.Public.class)
	@JsonProperty("shoot_id")
	@Column(name = "shoot_id", nullable = true)
	private Long shootId;
	
	@JsonView(Views.Public.class)
	private String address;
	
	@JsonView(Views.Public.class)
	private String city;
	
	@JsonView(Views.Public.class)
	private String description;
	
	@JsonView(Views.Public.class)
	private String notes;
}
