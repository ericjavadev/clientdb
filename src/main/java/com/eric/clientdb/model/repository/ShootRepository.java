package com.eric.clientdb.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eric.clientdb.model.Shoot;

@Repository
public interface ShootRepository extends CrudRepository<Shoot, Long> {

}
