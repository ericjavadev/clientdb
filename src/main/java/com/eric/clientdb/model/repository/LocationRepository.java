package com.eric.clientdb.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.eric.clientdb.model.Client;
import com.eric.clientdb.model.Location;

public interface LocationRepository extends CrudRepository<Location, Long> {
	// example query stub
	@Query("select l from Location l where l = :location")
	Location findByLocation(Location location);
}
