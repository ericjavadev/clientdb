package com.eric.clientdb.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.eric.clientdb.model.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

	// example query stub
	@Query("select c from Client c where c = :client")
	Client findByClient(Client client);
}
