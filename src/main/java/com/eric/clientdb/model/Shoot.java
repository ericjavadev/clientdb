package com.eric.clientdb.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.eric.clientdb.model.views.Views;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Entity
@Table(name = "shoot")
public class Shoot {

	@JsonView(Views.Public.class)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@JsonView(Views.Public.class)
	@JsonProperty("client_id")
	@Column(name = "client_id", nullable = true)
	private Long clientId;
	
	@JsonView(Views.Public.class)
	private Date date;
	
	@JsonView(Views.Public.class)
	@JsonProperty("length_in_minutes")
	@Column(name = "length_in_minutes", nullable = true)
	private long lengthInMinutes;
	
	@JsonView(Views.Public.class)
	private String theme;
	
	@JsonView(Views.Public.class)
	@JsonProperty("location_list")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "shoot_id")
	private List<Location> locationList;
}
