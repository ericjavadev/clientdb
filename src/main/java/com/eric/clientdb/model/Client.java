package com.eric.clientdb.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.eric.clientdb.model.views.Views;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Entity
@Table(name = "client",
		indexes = {@Index(name = "idx_name", columnList = "firstname,lastname", unique = true)})
public class Client {
	
	@JsonView(Views.Public.class)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@JsonView(Views.Public.class)
	@NotNull
	@Size(min=2, message="firstname should have atleast 2 characters")
	@Column(nullable = false)
	private String firstname;
	
	@JsonView(Views.Public.class)
	@NotNull
	@Size(min=2, message="lastname should have atleast 2 characters")
	@Column(nullable = false)
	private String lastname;
	
	@JsonView(Views.Public.class)
	private String nickname;
	
	@JsonView(Views.Public.class)
	private String phoneNumber;
	
	@Email
	@JsonView(Views.Public.class)
	private String email;
	
	@JsonView(Views.Public.class)
	private String facebook;
	
	@JsonView(Views.Public.class)
	private String instagram;
	
	@JsonView(Views.Public.class)
	private String twitter;
	
	@JsonView(Views.Public.class)
	@JsonProperty("shoot_list")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "client_id")
	private List<Shoot> shootList;
}
