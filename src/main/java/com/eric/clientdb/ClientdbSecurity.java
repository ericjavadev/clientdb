package com.eric.clientdb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class ClientdbSecurity extends WebSecurityConfigurerAdapter {
	
	@Value( "${api.username}" )
	private String apiUsername;
	
	@Value( "${api.password}" )
	private String apiPassword;
	
	// Authentication : User --> Roles
	protected void configure(AuthenticationManagerBuilder auth)
			throws Exception {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		auth.inMemoryAuthentication().passwordEncoder(encoder)
		.withUser(apiUsername).password(encoder.encode(apiPassword)).roles("USER");
	}

	// Authorization : Role -> Access
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable().headers().frameOptions().disable();
		http
			.httpBasic().and().authorizeRequests()
			.antMatchers("*", "/api/**").hasAnyRole("USER")
			.anyRequest().authenticated();
	}

}

